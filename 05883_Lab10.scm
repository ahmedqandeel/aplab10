; Qandeel Abbassi
; Class: BE(SE)-4B

;s: successor , z:zero, p: predecessor

;ADD
(define ADD
  (lambda (x y)
    (x s (y s z))
  )
)

;SUBTRACT
(define SUB
  (lambda (x y)
    (y p x)
  )
)

;AND
(define AND
  (lambda (X Y)
    (Y (X a b) b)
  )
)

;OR
(define OR
  (lambda (X Y)
    (Y a (X a b))
  )
)

;NOT
(define NOT
  (lambda (X)
    (X a b)
  )
)

;LEQ
(define LEQ
  (lambda (X Y)
    (IsZero (SUB X Y) )
  )
)

;GEQ
(define GEQ
  (lambda (X Y)
    (IsZero (SUB Y X) )
  )
)
